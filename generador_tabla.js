arr_titulos = ["Id", "no.Control", "Nombre"];

arr_datos = [ ["1", "09161133", "Banchez Cruz Pablo Zaid"], ["2", "09161166", "Sanchez Santiago Leonardo"],["3", "09167890", "Cruz Cruz Rosa"]];

function genera_tabla(titulos, datos) {
   tabla = document.createElement("table");
/* titulos */
	var encabezados = document.createElement("thead");
	var fila_encabezados = document.createElement("tr");
    var body = document.getElementsByTagName("body")[0];
/* datos */ 
  var tabla   = document.createElement("table");
  var tblBody = document.createElement("tbody");
 
  // Crea titulos
  
  for (var p=0; p< arr_titulos.length; p++){
		var encabezado = document.createElement("th");
		var texto_encabezado = document.createTextNode(arr_titulos[p]);
		encabezado.appendChild(texto_encabezado);
		fila_encabezados.appendChild(encabezado);
	}
	
 /* crear datos */
  for (var i = 0; i < arr_datos.length; i++) {
    var hilera = document.createElement("tr");
    for (var j = 0; j < arr_datos[i].length; j++) {
      var celda = document.createElement("td");
      var textoCelda = document.createTextNode(arr_datos[i][j]);
      celda.appendChild(textoCelda);
      hilera.appendChild(celda);
    }
     tblBody.appendChild(hilera);
  }
 
 tabla.appendChild(encabezados);
 encabezados.appendChild(fila_encabezados);
 tabla.appendChild(tblBody);
 body.appendChild(tabla);
 /* bordes de tabla */
 tabla.setAttribute("border", "2");
}

genera_tabla(arr_titulos, arr_datos);